# Create a VPC

## Create a Custom VPC with Subnets and Launch EC2 Instances

1. Create a custom VPC in the region of your choice. The VPC will need a:

   - A name
   - A CIDR block of IPv4 addresses
   - A `Name` tag

2. Create one or more **public** subnets within your VPC. Each subnet must have
   a CIDR block of addresses that is a **subsection** of its VPC's CIDR block.
   In order to be a **public** subnet, you must:

   - Enable the `Auto-assign public IPv4 addresses` (and optionally IPv6) option
     on the subnet

3. Create an **internet gateway** and associate it with your VPC.

4. Connect the VPC to the internet by associating the VPC's **main route table**
   with the **internet gateway**:

   - Create a new route in the route table that routes **all IPv4 traffic**
     (`0.0.0.0/0`) and optionally IPv6 traffic to the internet gateway

5. Create one or more **private** subnets within your VPC. Each subnet must have
   a CIDR block of addresses that is a **subsection** of its VPC's CIDR block.
   Do **not** enable auto-assign of public IPv4/IPv6 addresses.

6. Create at least one **NAT Gateway** inside one of your subnets. The private
   instances will use the NAT gateway to communicate with the public internet
   (download software, etc.).

7. Create a new **private** route table. Associcate `0.0.0.0/0` with the **NAT
   Gateway** you created in step 6.

8. Create a security group for your **public** instances with the following
   **inbound** rules:

   - SSH on port 22 from [desired ip address range] for IPv4 and IPv6
   - HTTP on port 80 from anywhere for IPv4 and IPv6
   - HTTPS on port 443 from anywhere for IPv4 and IPv6

9. Create a security group for your **private** instances with the following
   **inbound** rules:

   - SSH on port 22 from **public** security group you created in step 8

10. Launch one or more EC2 instances in your public subnet(s). Attach the
    **public** security group to the instance(s).

11. Launch one or more EC2 instances in your private subnet(s). Attach the
    **private** security group to the instance(s).

12. SSH into one of your **public** instances using its **public** IPv4 address.
    From the public instance, SSH into one of your **private** instances using
    its **private** IPv4 address. Note that attempting to SSH into a **private**
    instance from **outside a public instance** is **blocked** by your firewall
    rules.

13. From your **private** instance, run the following command:
    - `ping google.com` Note that traffic is flowing to Google **across the
      internet** through the **NAT Gateway** from the **private** instance.

## Create an Elastic Network Interface and Attach an Elastic IP

1. Create an Elastic Network Interface (ENI) in the **public** subnet of your
   choice. Auto-assign private IPv4 and (optionally) IPv6 addresses. Associate
   public web server and ssh security groups.

2. Associate the ENI with an Elastic IP address in the **same Availability
   Zone**. Note that the `Elastic IP address owner` property of the ENI is now
   populated.

3. Attach the ENI to a new **public** instance in **any** subnet in the VPC.

## Create Application Load Balancer

1. Create new VPC and 1 or more subnets. Attach an internet gateway to your VPC.
   Point the routes of the main route table for the VPC to direct all
   non-internal traffic to the internet gateway. Create security group(s) to
   allow inbound SSH and HTTP/S traffic.

2. Create a Launch Template with your desired instance type and size. Create an
   Auto-scaling group that utilizes your launch template. Creating the
   auto-scaling group should automatically provision EC2 instances in your
   chosen subnet(s).

3. Create a Target Group. Register your EC2 instances with the target group.

4. Create an Application Load Balancer (ALB). Forward traffic coming to your ALB
   on port 80 to your Target Group.

5. Create a new **record** in your **Route 53 hosted zone**. The record should
   be an alias, and route to the ALB you created in step 4.
