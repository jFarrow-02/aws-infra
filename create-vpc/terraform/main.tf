terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "4.33.0"
      }
    }
}

variable "vpc-cidr-block" {
  type = string
  description = "CIDR block for VPC"
}

variable "subnet-cidr-blocks" {
    type = list(string)
    description = "CIDR blocks for subnets"
}

provider "aws" {
    region = "us-east-1"
    profile = "sysAdmin02"
}

resource "aws_vpc" "development-vpc-01" {
    
    cidr_block = var.vpc-cidr-block
    tags = {
        Name: "development-vpc-01",
        Env: "dev"
    }
}

resource "aws_internet_gateway" "ig-dev-vpc-01" {
    vpc_id = aws_vpc.development-vpc-01.id
    tags = {
        Name: "ig-dev-vpc-01"
        Env: "dev"
    }
}

resource "aws_route_table" "rtb-main-dev-vpc-01" {
    vpc_id = aws_vpc.development-vpc-01.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.ig-dev-vpc-01.id
    }
    tags = {
        Name: "rtb-main-dev-vpc-01"
        Env: "dev"
    }
}

resource "aws_route_table_association" "rtb-main-subnet-assoc" {
    gateway_id = aws_internet_gateway.ig-dev-vpc-01.id
}
resource "aws_subnet" "sn-public-1f-dev-vpc-01" {
    vpc_id = aws_vpc.development-vpc-01.id
    cidr_block = var.subnet-cidr-blocks[0]
    availability_zone = "us-east-1f"
    tags = {
        Name: "sn-public-1f-dev-vpc-01"
        Env: "dev"
    }
}

resource "aws_subnet" "sn-public-1e-dev-vpc-01" {
    vpc_id = aws_vpc.development-vpc-01.id
    cidr_block = var.subnet-cidr-blocks[1]
    availability_zone = "us-east-1e"
    tags = {
        Name: "sn-public-1e-dev-vpc-01"
        Env: "dev"
    }
}

resource "aws_s3_bucket" "languid-cranberry" {
    bucket = "languid-cranberry-us-east-1"
    tags = {
        Name = "s3-languid-cranberry-us-east-1"
        Env: "dev"
    }
}