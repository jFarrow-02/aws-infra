provider "aws" {
  region = "us-east-1"
}

variable "env-prefix" {
  type = string
  description = "Environment prefix"
}

variable "instance-type" {
  type = string
  description = "EC2 instance type"
}

variable "private-key-location" {
  type = string
  description = "Private key location"
}
variable "public-key-location" {
  type = string
  description = "Public key location"
}
variable "subnet-list" {
  type = list(object({cidr_block = string, name = string, az = string}))
}
variable "vpc-cidr-block" {
  type = string
  description = "CIDR block for VPC"
}
resource "aws_vpc" "demovpc-01" {
  cidr_block = var.vpc-cidr-block
  tags = {
    Name: "${var.env-prefix}-vpc"
  }
}

resource "aws_internet_gateway" "igw-demovpc-01" {
  vpc_id = aws_vpc.demovpc-01.id
  tags = {
    Name: "${var.env-prefix}-igw"
  }
}
resource "aws_subnet" "sn-public-1a" {
  vpc_id = aws_vpc.demovpc-01.id
  cidr_block = var.subnet-list[0].cidr_block
  availability_zone = var.subnet-list[0].az
  tags = {
    Name: "${var.env-prefix}-${var.subnet-list[0].name}"
  }
}

resource "aws_subnet" "sn-public-1b" {
  vpc_id = aws_vpc.demovpc-01.id
  cidr_block = var.subnet-list[1].cidr_block
  availability_zone = var.subnet-list[1].az
  tags = {
    Name: "${var.env-prefix}-${var.subnet-list[1].name}"
  }
}

resource "aws_subnet" "sn-public-1c" {
  vpc_id = aws_vpc.demovpc-01.id
  cidr_block = var.subnet-list[2].cidr_block
  availability_zone = var.subnet-list[2].az
  tags = {
    Name: "${var.env-prefix}-${var.subnet-list[2].name}"
  }
}

resource "aws_subnet" "sn-public-1d" {
  vpc_id = aws_vpc.demovpc-01.id
  cidr_block = var.subnet-list[3].cidr_block
  availability_zone = var.subnet-list[3].az
  tags = {
    Name: "${var.env-prefix}-${var.subnet-list[3].name}"
  }
}

resource "aws_route_table" "rtb-public-demovpc-01" {
  vpc_id = aws_vpc.demovpc-01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-demovpc-01.id
  }

  tags = {
      Name: "rtb-public-demovpc-01"
    }
}

resource "aws_route_table_association" "rtb-sn-public1a" {
  route_table_id = aws_route_table.rtb-public-demovpc-01.id
  subnet_id = aws_subnet.sn-public-1a.id
}

resource "aws_route_table_association" "rtb-sn-public1b" {
  route_table_id = aws_route_table.rtb-public-demovpc-01.id
  subnet_id = aws_subnet.sn-public-1b.id
}

resource "aws_route_table_association" "rtb-sn-public1c" {
  route_table_id = aws_route_table.rtb-public-demovpc-01.id
  subnet_id = aws_subnet.sn-public-1c.id
}

resource "aws_route_table_association" "rtb-sn-public1d" {
  route_table_id = aws_route_table.rtb-public-demovpc-01.id
  subnet_id = aws_subnet.sn-public-1d.id
}

resource "aws_security_group" "sg-public-webserver-ssh" {
  name = "${var.env-prefix}-public-webserver-ssh"
  vpc_id = aws_vpc.demovpc-01.id
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name: "sg-public-webserver-ssh"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  public_key = file(var.public-key-location)
}

resource "aws_instance" "ec2-public-1a" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance-type
  subnet_id = aws_subnet.sn-public-1a.id
  vpc_security_group_ids = [aws_security_group.sg-public-webserver-ssh.id]
  availability_zone =  "us-east-1a"
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = var.private-key-location
  }

  tags = {
    Name = "${var.env-prefix}-ec2-public-1a"
  }
}

resource "aws_instance" "ec2-public-1b" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance-type
  subnet_id = aws_subnet.sn-public-1b.id
  vpc_security_group_ids = [aws_security_group.sg-public-webserver-ssh.id]
  availability_zone =  "us-east-1b"
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = var.private-key-location
  }

  tags = {
    Name = "${var.env-prefix}-ec2-public-1b"
  }
}

resource "aws_instance" "ec2-public-1c" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance-type
  subnet_id = aws_subnet.sn-public-1c.id
  vpc_security_group_ids = [aws_security_group.sg-public-webserver-ssh.id]
  availability_zone =  "us-east-1c"
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = var.private-key-location
  }

  tags = {
    Name = "${var.env-prefix}-ec2-public-1c"
  }
}

resource "aws_instance" "ec2-public-1d" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance-type
  subnet_id = aws_subnet.sn-public-1d.id
  vpc_security_group_ids = [aws_security_group.sg-public-webserver-ssh.id]
  availability_zone =  "us-east-1d"
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = var.private-key-location
  }

  tags = {
    Name = "${var.env-prefix}-ec2-public-1d"
  }
}